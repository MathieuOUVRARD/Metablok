using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour
{
    public int nrows;
    public int ncolumns;
    public bool done = false;
    public Cell cell;
    public Cell virtualCell;

    private Vector3 rowVector = new Vector3(0, 0, 1);
    private Vector3 columnVector = new Vector3(1, 0, 0);
    private Cell[,] cellsGrid;
    private List<Cell> cellsList = new List<Cell>();


    // Update is called once per frame

    void Start()
    {
        cellsGrid = new Cell[ncolumns, nrows];
        float nCells = ncolumns * nrows;

        GameObject container = Instantiate(new GameObject("Grid"), Vector3.zero, Quaternion.identity);

        if (!done)
        {
            for (int i = 0; i < nrows; i++)
            {
                for (int j = 0; j < ncolumns; j++)
                {
                    Cell cellToAdd = Instantiate<Cell>(cell, this.transform.position + (i * rowVector) + (j * columnVector), cell.transform.rotation, container.transform);
                    cellToAdd.gameObject.name = "Cell " + j + ":" + i;
                    cellsGrid[j, i] = cellToAdd;
                    cellsList.Add(cellToAdd);
                }
            }
            LookForNeighbours();

            done = true;
        }
    }

    public void LookForNeighbours()
    {
        for (int i = 0; i < nrows; i++)
        {
            for (int j = 0; j < ncolumns; j++)
            {
                if (i == 0)
                {
                    cellsGrid[j, i].downNeighbour = virtualCell;
                }
                else
                {
                    cellsGrid[j, i].downNeighbour = cellsGrid[j, i - 1];
                }

                if (i == nrows-1)
                {
                    cellsGrid[j, i].upNeighbour = virtualCell;
                }
                else
                {
                    cellsGrid[j, i].upNeighbour = cellsGrid[j, i + 1];
                }

                if (j == 0)
                {
                    cellsGrid[j, i].leftNeighbour = virtualCell;
                }
                else
                {
                    cellsGrid[j, i].leftNeighbour = cellsGrid[j - 1, i];
                }

                if (j == ncolumns-1)
                {
                    cellsGrid[j, i].rightNeighbour = virtualCell;
                }
                else
                {
                    cellsGrid[j, i].rightNeighbour = cellsGrid[j + 1, i];
                }
            }
        }
    }
}
