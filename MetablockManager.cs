using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetablockManager : MonoBehaviour
{
    public static MetablockManager instance;
    public Material blockMaterial;

    private List<Cell> Blocks;
    private ShapesManager shapes;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        Blocks = new List<Cell>();
        Blocks.Clear();
        shapes = ShapesManager.instance;
    }

    private void Update()
    {
        
    }

    public void NewBlock(Cell cell)
    {
        cell.currentShape = Instantiate(shapes._1Block, cell.transform.position + new Vector3(0f, 0.5f, 0f), shapes._1Block.transform.rotation);

        UpdateCellNeighborhood();
        cell.CheckForBlockNeighbours();
        if (cell.nblockNeighbours>0)
        {
            SearchForShape(cell);
        }

        Blocks.Add(cell);
    }

    internal void DeleteBlock(Cell cell)
    {
        if(cell.currentShape.CompareTag("BaseBlock"))
        {
            Destroy(cell.currentShape.gameObject);
        }

        else
        {
            Shape shapeToDestroy = cell.currentShape;
            cell.currentShape = null;

            for(int i=0;i< shapeToDestroy.cellsOccupied.Count;i++)
            {
                shapeToDestroy.cellsOccupied[i].buildable = true;
                if(shapeToDestroy.cellsOccupied[i] != cell)
                {
                    NewBlock(shapeToDestroy.cellsOccupied[i]);
                }                
            }            
            Destroy(shapeToDestroy.gameObject);
        }

        Blocks.Remove(cell);
    }

    public void SearchForShape(Cell cell)
    {
        List<Cell> cellsToProceed = new List<Cell>();

        Cell rightNeighbour = cell.rightNeighbour;
        Cell leftNeighbour = cell.leftNeighbour;
        Cell upNeighbour = cell.upNeighbour;
        Cell downNeighbour = cell.downNeighbour;

        Cell upRightNeighbour = upNeighbour.rightNeighbour;
        Cell upLeftNeighbour = upNeighbour.leftNeighbour;
        Cell downRightNeighbour = downNeighbour.rightNeighbour;
        Cell downLeftNeighbour = downNeighbour.leftNeighbour;


        bool testRightNeighbour     = (rightNeighbour.currentShape != null && rightNeighbour.buildable);
        bool testLeftNeighbour      = (leftNeighbour.currentShape != null && leftNeighbour.buildable);
        bool testUpNeighbour        = (upNeighbour.currentShape != null && upNeighbour.buildable);
        bool testDownNeighbour      = (downNeighbour.currentShape != null && downNeighbour.buildable);

        bool testUpRightNeighbour   = (upRightNeighbour.currentShape != null && upRightNeighbour.buildable);
        bool testUpLeftNeighbour    = (upLeftNeighbour.currentShape != null && upLeftNeighbour.buildable);
        bool testDownRightNeighbour = (downRightNeighbour.currentShape != null && downRightNeighbour.buildable);
        bool testDownLeftNeighbour  = (downLeftNeighbour.currentShape != null && downLeftNeighbour.buildable);

        Quaternion rotate90 = Quaternion.Euler(0, 90, 0);
        Vector3 lift05 = new Vector3(0f, 0.5f, 0f);


        // 3 block wall     //..................................................................................
        {
            // Right
            if (testRightNeighbour &&
                rightNeighbour.rightNeighbour.currentShape != null && rightNeighbour.rightNeighbour.buildable)
            {
                cellsToProceed.Add(cell);
                cellsToProceed.Add(rightNeighbour);
                cellsToProceed.Add(rightNeighbour.rightNeighbour);

                Wall newShape = Instantiate(shapes._3BlockWall, rightNeighbour.transform.position + lift05, shapes._3BlockWall.transform.rotation);
                newShape.cellsOccupied = new List<Cell>(cellsToProceed);

                ProceedCell(cellsToProceed, newShape);        
                return;
            }
            // Left
            
            if (testLeftNeighbour &&
                leftNeighbour.leftNeighbour.currentShape != null && leftNeighbour.leftNeighbour.buildable)
            {
                cellsToProceed.Add(cell);
                cellsToProceed.Add(leftNeighbour);
                cellsToProceed.Add(leftNeighbour.leftNeighbour);

                Wall newShape = Instantiate(shapes._3BlockWall, leftNeighbour.transform.position + lift05, shapes._3BlockWall.transform.rotation);
                newShape.size = 3;
                newShape.cellsOccupied = new List<Cell>(cellsToProceed);

                ProceedCell(cellsToProceed, newShape);
                return;
            }
            // Up
            if (testUpNeighbour &&
                upNeighbour.upNeighbour.currentShape != null && upNeighbour.upNeighbour.buildable)
            {
                cellsToProceed.Add(cell);
                cellsToProceed.Add(upNeighbour);
                cellsToProceed.Add(upNeighbour.upNeighbour);

                Wall newShape = Instantiate(shapes._3BlockWall, upNeighbour.transform.position + lift05, shapes._3BlockWall.transform.rotation * rotate90);
                newShape.size = 3;
                newShape.cellsOccupied = new List<Cell>(cellsToProceed);

                ProceedCell(cellsToProceed, newShape);
                return;
            }
            // Down
            if (testDownNeighbour &&
                downNeighbour.downNeighbour.currentShape != null && downNeighbour.downNeighbour.buildable)
            {
                cellsToProceed.Add(cell);
                cellsToProceed.Add(downNeighbour);
                cellsToProceed.Add(downNeighbour.downNeighbour);

                Wall newShape = Instantiate(shapes._3BlockWall, downNeighbour.transform.position + lift05, shapes._3BlockWall.transform.rotation * rotate90);
                newShape.size = 3;
                newShape.cellsOccupied = new List<Cell>(cellsToProceed);

                ProceedCell(cellsToProceed, newShape);
                return;
            }
            // Middle V
            if (testUpNeighbour && testDownNeighbour)
            {
                cellsToProceed.Add(upNeighbour);
                cellsToProceed.Add(cell);                
                cellsToProceed.Add(downNeighbour);

                Wall newShape = Instantiate(shapes._3BlockWall, cell.transform.position + lift05, shapes._3BlockWall.transform.rotation * rotate90);
                newShape.size = 3;
                newShape.cellsOccupied = new List<Cell>(cellsToProceed);

                ProceedCell(cellsToProceed, newShape);
                return;
            }
            // Middle H
            if (testLeftNeighbour && testRightNeighbour)
            {
                cellsToProceed.Add(leftNeighbour);
                cellsToProceed.Add(cell);
                cellsToProceed.Add(rightNeighbour);

                Wall newShape = Instantiate(shapes._3BlockWall, cell.transform.position + lift05, shapes._3BlockWall.transform.rotation);
                newShape.size = 3;
                newShape.cellsOccupied = new List<Cell>(cellsToProceed);

                ProceedCell(cellsToProceed, newShape);
                return;
            }
        }

        // 4 block square   //..................................................................................
        {            
            //  botom left
            if (testRightNeighbour && testUpNeighbour && testUpRightNeighbour)
            {
                cellsToProceed.Add(cell);
                cellsToProceed.Add(rightNeighbour);
                cellsToProceed.Add(upNeighbour);
                cellsToProceed.Add(upRightNeighbour);

                Shape newShape = Instantiate(shapes._4BlockCube, cell.transform.position + new Vector3(.5f, 0.5f, .5f),
                    shapes._4BlockCube.transform.rotation);
                newShape.cellsOccupied = new List<Cell>(cellsToProceed);

                ProceedCell(cellsToProceed, newShape);
                return;
            }
            //  botom right
            if (testLeftNeighbour && testUpNeighbour && testUpLeftNeighbour)
            {
                cellsToProceed.Add(cell);
                cellsToProceed.Add(leftNeighbour);
                cellsToProceed.Add(upNeighbour);
                cellsToProceed.Add(upLeftNeighbour);

                Shape newShape = Instantiate(shapes._4BlockCube, cell.transform.position + new Vector3(-.5f, 0.5f, .5f),
                    shapes._4BlockCube.transform.rotation);
                newShape.cellsOccupied = new List<Cell>(cellsToProceed);

                ProceedCell(cellsToProceed, newShape);
                return;
            }
            //  top left
            if (testRightNeighbour && testDownNeighbour && testDownRightNeighbour)
            {
                cellsToProceed.Add(cell);
                cellsToProceed.Add(rightNeighbour);
                cellsToProceed.Add(downNeighbour);
                cellsToProceed.Add(downRightNeighbour);

                Shape newShape = Instantiate(shapes._4BlockCube, cell.transform.position + new Vector3(.5f, 0.5f, -.5f),
                    shapes._4BlockCube.transform.rotation);
                newShape.cellsOccupied = new List<Cell>(cellsToProceed);

                ProceedCell(cellsToProceed, newShape);
                return;
            }
            //  top right
            if (testLeftNeighbour && testDownNeighbour && testDownLeftNeighbour)
            {
                cellsToProceed.Add(cell);
                cellsToProceed.Add(leftNeighbour);
                cellsToProceed.Add(downNeighbour);
                cellsToProceed.Add(downLeftNeighbour);

                Shape newShape = Instantiate(shapes._4BlockCube, cell.transform.position + new Vector3(-.5f, 0.5f, -.5f),
                    shapes._4BlockCube.transform.rotation);
                newShape.cellsOccupied = new List<Cell>(cellsToProceed);

                ProceedCell(cellsToProceed, newShape);
                return;
            }
        }
    }    

    public void ProceedCell(List<Cell> cellsToProceed, Shape newShape)
    {
        for (int i = 0; i < cellsToProceed.Count; i++)
        {
            Destroy(cellsToProceed[i].currentShape.gameObject);
            cellsToProceed[i].buildable = false;
            cellsToProceed[i].currentShape = newShape;
        }
    }

    public void UpdateCellNeighborhood()
    {
        for (int i = 0; i < Blocks.Count; i++)
        {
            Blocks[i].CheckForBlockNeighbours();
        }
    }

    public void ExploreShape(Cell firstCell)
    {
        List<Cell> waitingLine = new List<Cell>();
        List<Cell> shape = new List<Cell>();
        Cell cellToEvaluate;
        waitingLine.Add(firstCell);        
        
        while (waitingLine.Count != 0)
        {
            cellToEvaluate = waitingLine[0];
            waitingLine.Remove(cellToEvaluate);
            List<Cell> neighbors = new List<Cell>(cellToEvaluate.CheckForBlockNeighbours());
            for(int i=0; i<neighbors.Count; i++)
            {
                waitingLine.Add(neighbors[i]);
            }
            shape.Add(cellToEvaluate);
        }
    }
}