using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    [Header("Neighbors")]
    public Cell leftNeighbour;
    public Cell rightNeighbour;
    public Cell upNeighbour;
    public Cell downNeighbour;
    public float nblockNeighbours;

    [Header("Shape")]
    public Shape currentShape;
    public bool buildable = true;

    private MetablockManager blockManager;
    private Material baseMaterial;

    // Start is called before the first frame update
    void Start()
    {
        blockManager = MetablockManager.instance;
        baseMaterial = this.GetComponent<Renderer>().material;
        nblockNeighbours = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                //Select stage    
                if (hit.transform.gameObject == this.gameObject)
                {
                    if(currentShape == null)
                    {
                        blockManager.NewBlock(this);
                    }        
                }
            }
        }
        
        if (currentShape != null  && Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                //Select stage    
                if (hit.transform.gameObject == this.gameObject)
                {                    
                    blockManager.DeleteBlock(this);                    
                }
            }
        }
    }

    public List<Cell> CheckForBlockNeighbours()
    {
        List<Cell> toReturn = new List<Cell>();
        nblockNeighbours = 0;

        if (downNeighbour.currentShape != null && downNeighbour.currentShape.CompareTag("BaseBlock"))
        {
            toReturn.Add(downNeighbour);
            nblockNeighbours++;
        }
        if (rightNeighbour.currentShape && rightNeighbour.currentShape.CompareTag("BaseBlock"))
        {
            toReturn.Add(rightNeighbour);
            nblockNeighbours++;
        }
        if (upNeighbour.currentShape != null && upNeighbour.currentShape.CompareTag("BaseBlock"))
        {
            toReturn.Add(upNeighbour);
            nblockNeighbours++;
        }
        if (leftNeighbour.currentShape != null && leftNeighbour.currentShape.CompareTag("BaseBlock"))
        {
            toReturn.Add(leftNeighbour);
            nblockNeighbours++;
        }

        return toReturn; 
    }
}
