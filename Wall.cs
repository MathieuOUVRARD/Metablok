using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : Shape
{
    public float size;
    public static float sizeMax = 4;
    public List<Cell> augmentationCells;

    private ShapesManager shapesManager;   
    
    // Start is called before the first frame update
    void Start()
    {
        shapesManager = ShapesManager.instance;
        size = cellsOccupied.Count;

        if(size < sizeMax)
        {
            DefineAugmentationCells();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(size<sizeMax)
        {
            CheckForAugmentation();
        }
    }

    public void CheckForAugmentation()
    {
        if (augmentationCells[0].buildable && augmentationCells[0].currentShape != null)
        {
            cellsOccupied.Add(augmentationCells[0]);
            augmentationCells[0].buildable = false;

            Vector3 offset = (augmentationCells[0].transform.position - cellsOccupied[0].transform.position).normalized;
            Wall newWall = Instantiate(shapesManager._4BlockWall, cellsOccupied[0].currentShape.transform.position + offset/2, 
                cellsOccupied[0].currentShape.transform.rotation);;
            newWall.cellsOccupied = new List<Cell>(cellsOccupied);

            for(int i =0; i<cellsOccupied.Count; i++)
            {
                Destroy(cellsOccupied[i].currentShape.gameObject);
                cellsOccupied[i].currentShape = newWall;
            }                        
            return;
        }

        if (augmentationCells[1].buildable && augmentationCells[1].currentShape != null)
        {
            cellsOccupied.Add(augmentationCells[1]);
            augmentationCells[1].buildable = false;
            Vector3 offset = (augmentationCells[1].transform.position - cellsOccupied[0].transform.position).normalized;

            Wall newWall = Instantiate(shapesManager._4BlockWall, cellsOccupied[0].currentShape.transform.position + offset/2,
                cellsOccupied[0].currentShape.transform.rotation);
            newWall.cellsOccupied = new List<Cell>(cellsOccupied);

            for (int i = 0; i < cellsOccupied.Count; i++)
            {
                Destroy(cellsOccupied[i].currentShape.gameObject);
                cellsOccupied[i].currentShape = newWall;
            }
            return;
        }
    }

    private void DefineAugmentationCells()
    {
        Vector3 orientation = (cellsOccupied[0].transform.position - cellsOccupied[(int)size - 1].transform.position).normalized;
        if (orientation == new Vector3(1, 0, 0) || orientation == new Vector3(-1, 0, 0))
        {
            if (orientation == new Vector3(1, 0, 0))
            {
                augmentationCells.Add(cellsOccupied[0].rightNeighbour);
                augmentationCells.Add(cellsOccupied[(int)size - 1].leftNeighbour);
            }
            else
            {
                augmentationCells.Add(cellsOccupied[0].leftNeighbour);
                augmentationCells.Add(cellsOccupied[(int)size - 1].rightNeighbour);
            }
        }
        else
        {
            if (orientation == new Vector3(0, 0, 1))
            {
                augmentationCells.Add(cellsOccupied[0].upNeighbour);
                augmentationCells.Add(cellsOccupied[(int)size - 1].downNeighbour);
            }
            else
            {
                augmentationCells.Add(cellsOccupied[0].downNeighbour);
                augmentationCells.Add(cellsOccupied[(int)size - 1].upNeighbour);
            }
        }
    }
}
