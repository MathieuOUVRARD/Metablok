using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapesManager : MonoBehaviour
{
    public static ShapesManager instance;

    public Shape _1Block;
    public Wall _3BlockWall;
    public Wall _4BlockWall;
    public Shape _4BlockCube;

    private void Awake()
    {
        instance = this;
    }
}
